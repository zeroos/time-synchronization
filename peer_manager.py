import logging
import random

from timer import Timer
from peer import Peer


class PeerManager:
    """
    Klasa używana do przechowywania i aktualizowania informacji
    o partnerach.
    """
    peers = {}
    incoming = []
    min_connected_peers = 2

    def __init__(self, my_uuid):
        self.my_uuid = my_uuid

    @staticmethod
    def service_name2uuid(service_name):
        """
        Metoda używana do pobrania identyfikatora uuid zawartego w service_name.
        """
        return service_name[:service_name.index('.')]

    def get_accept_callback(self):
        """
        Zwraca `accept_callback` do wykorzystania przez discover_services(),
        który zostanie wywołany, gdy inny komputer spróbuje się z nami
        połączyć.
        """
        def accept_callback(socket, addr):
            logging.debug("accepting connection from: {}".format(addr))
            ip, port = addr
            uuid = "i"+str(len(self.peers))
            host_name = uuid
            self.incoming.append(Peer(uuid,
                                      host_name,
                                      ip,
                                      port,
                                      socket))

        return accept_callback

    def get_add_callback(self):
        """
        Zwraca `add_callback` do wykorzystania przez discover_services(),
        który zostanie wywołany, gdy nowa usługa pojawi się w sieci.
        """
        def add_callback(service_name, host_name, ip, port):
            logging.debug("Service added:\n" +
                          "    service name: {}\n".format(service_name) +
                          "    host name:    {}\n".format(host_name) +
                          "    ip:    {}\n".format(ip) +
                          "    port:         {}".format(port))
            uuid = self.service_name2uuid(service_name)
            assert uuid not in self.peers
            me = True if uuid == self.my_uuid else False
            self.peers[uuid] = Peer(uuid, host_name, ip, port,
                                    me=me)
        return add_callback

    def get_remove_callback(self):
        """
        Zwraca `remove_callback` do wykorzystania przez discover_services().
        """
        def remove_callback(uuid):
            logging.debug("Service deleted:\n    {}".format(uuid))
            self.peers.pop(uuid)
        return remove_callback

    def __str__(self):
        result = ""
        for i, peer in enumerate(self.peers.values()):
            result += "{}. {}\n".format(i+1, peer)
        result += "- incoming:\n"
        for i, peer in enumerate(self.incoming):
            result += "{}. {}\n".format(i+1, peer)
        result += "----------\n"
        result += "Total: {} other peers, {} in, {} connected".format(
            len(self.others()),
            len(self.incoming),
            len(self.connected_peers())
        )
        return result

    def get_peer_by_addr(self, ip, port):
        """
        Zwraca partnera nasłuchującego na danym adresie IP i porcie.
        """
        l = [p for p in self.others() if p.ip == ip and p.port == port]
        assert len(l) < 2
        if len(l) == 1:
            return l[0]
        return None

    def others(self):
        """
        Zwraca listę wszystkich partnerów bez tego, który reprezentuje dany
        proces.
        """
        return [p for p in self.peers.values() if not p.me]

    def connected_peers(self):
        """
        Zwraca listę wszystkich połączonych partnerów.
        """
        return [p for p in self.others() if p.connected()]

    def not_connected_peers(self):
        """
        Zwraca listę wszystkich niepołączonych partnerów.
        """
        return [p for p in self.others() if not p.connected()]

    def refresh(self):
        """
        Odświeża listę partnerów. W tej funkcji następuje również
        synchronizacja czasu z losowo wybranym partnerem oraz
        uaktualnianie zegara.
        """
        while (len(self.connected_peers()) < self.min_connected_peers and
               len(self.not_connected_peers()) > 0):
            logging.debug("Init connection from refresh")
            peer = random.choice(self.not_connected_peers())
            peer.init_connection()

        r = random.choice(range(100))
        if r < 60 and len(self.connected_peers()) > 0:
            logging.debug("random: {}".format(r))
            peer = random.choice(self.connected_peers())
            peer.sync_request()

        elif r < 70:
            tds = [p.time() for p in self.connected_peers()
                   if p.time_difference is not None and
                   p.time() > 15]

            if len(tds) > 0:
                avg = sum(tds)/len(tds)
                Timer.t = avg
                for p in self.connected_peers():
                    p.time_difference = None
