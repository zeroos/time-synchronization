import time
from threading import Thread


class Timer(Thread):
    """
    Klasa reprezentująca zegar danego hosta (partnera).
    """
    t = 0

    def run(self):
        """
        Funkcja uruchamia zegar.
        """
        while True:
            Timer.t += 1
            time.sleep(1)

    @staticmethod
    def get_time():
        """
        Funkcja zwraca aktualną wartość zegara.
        """
        return Timer.t
