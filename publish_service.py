import logging
import select
import socket
import sys
import pybonjour
from uuid import uuid4

def get_my_ip():
    return socket.gethostbyname(socket.gethostname())

def publish_service(port=12345, name=None, regtype="_timesync._tcp"):
    """
    Funkcja służy do zarejestrowania usługi w Bonjour. Usługa
    powinna być wcześniej uaktywniona i nasłuchiwać na odpowiednim
    porcie.
    """
    registered = False

    if name is None:
        name = str(uuid4())

    name += "--" + get_my_ip().replace('.', '-')

    def register_callback(sdRef, flags, errorCode, name, regtype, domain):
        nonlocal registered  # NOQA
        if errorCode == pybonjour.kDNSServiceErr_NoError:
            logging.debug('Registered service:')
            logging.debug('  name    = {}'.format(name))
            logging.debug('  regtype = {}'.format(regtype))
            logging.debug('  domain  = {}'.format(domain))
            registered = True
        else:
            logging.error('Error {}, service not registered'.format(errorCode))

    sdRef = pybonjour.DNSServiceRegister(name=name,
                                         regtype=regtype,
                                         port=port,
                                         callBack=register_callback)

    while not registered:
        ready = select.select([sdRef], [], [], 5)
        if sdRef in ready[0]:
            pybonjour.DNSServiceProcessResult(sdRef)

    if not registered:
        raise Exception("Couldn't create Bonjour service")
    else:
        return name, sdRef

if __name__ == "__main__":
    """ Serves as an example, registers multiple test services """
    sdrefs = []
    for i in range(10):
        name, sdref = publish_service(1234+i)
        sdrefs.append(sdref)

    print("Press ENTER to exit")
    input()
    for sdref in sdrefs:
        sdref.close()
