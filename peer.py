import logging
import socket
from select import select
import struct
from timer import Timer
import threading
from math import sqrt
from termcolor import colored

MSG_TYPES = {
    "S": "sync-req",
    "R": "sync-repl",
    "T": "sync-thx",
}

SYNC_TIMEOUT = 3
CONNECTION_DIED_TIMEOUT = 30
LAST_LATENCIES_QUANTITY = 10


class Peer:
    """
    Klasa reprezentuje partnera.
    """

    def __init__(self, uuid, host_name, ip, port, sock=None, me=False):
        """ Konstruktor, inicjalizuje dane na temat partnera. """
        self.uuid = uuid
        self.host_name = host_name
        self.ip = ip
        self.port = port
        self.socket = None
        self.me = me
        self.time_difference = None
        self.connection_average_time = 0.0
        self.connection_quality = 0.0
        self.last_latencies = []
        self._sync_data = []
        self._current_sync_data = {"me": {}, "they": {}}
        self.thread = None
        if sock is not None:
            self.init_connection(sock)
        if me:
            self.time_difference = 0

    def run(self):
        """
        Wątek słuchający zapytań od innych partnerów i odpowiadający na nie.
        """
        logging.debug("Starting thread for {}".format(self.host_name))

        try:
            while(self.connected()):
                ready = select([self.socket], [], [], CONNECTION_DIED_TIMEOUT)
                # logging.debug("debug: {}".format(repr(ready)))
                if ready[0]:
                    self.process_recv()
                else:
                    self.close_connection()
        except socket.error:
            logging.warning("Socket died")
            self.close_connection()
        logging.debug("Thread for {} closed".format(self.host_name))

    def init_connection(self, sock=None):
        """ Inicjuje połączenie z partnerem. """
        assert not self.me
        if self.socket is not None:
            logging.warning("Cannot initialize connection -- connection " +
                            "already initialized.")
            return

        try:
            if sock is None:
                logging.debug("Trying to connect to {}:{}".format(
                              self.host_name, self.port))
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.connect((self.ip, self.port))
                logging.debug("Connected to {}:{}".format(
                    self.host_name, self.port))
            else:
                self.socket = sock

            if self.thread is None or not self.thread.is_alive():
                self.thread = threading.Thread(target=self.run)
                self.thread.start()
        except socket.error as err:
            logging.debug("Unable to connect to {}:{} :\n{}".format(
                self.host_name, self.port, str(err)))
            self.close_connection()

    def close_connection(self):
        """ Zamyka połączenie z partnerem. """
        logging.debug("Closing connection with {}.".format(self.host_name))
        if self.connected():
            self.socket.close()
            self.socket = None
        else:
            logging.warning("Tried to close connection with {} that" +
                            "was not open.".format(self.host_name))

    def update_connection_stats(self):
        """
        Uaktualnia informacje dotyczące połączenia z danym partnerem, to znaczy średni czas potrzebny na dotarcie komunikatu synchronizującego
        oraz jakość połączenia, tj. odchylenie standardowe ostatnich czasów (maksymalna liczba jest określona przez LAST_LATENCIES_QUANTITY).
        """
        if len(self.last_latencies) > LAST_LATENCIES_QUANTITY:
            del self.last_latencies[:len(self.last_latencies) - LAST_LATENCIES_QUANTITY]
        self.connection_average_time = sum(self.last_latencies) / float(len(self.last_latencies))
        self.connection_quality = sqrt(sum([(t - self.connection_average_time)**2 for t in self.last_latencies]) / float(len(self.last_latencies)))

    def get_status(self):
        """
        Zwraca aktualny stan partnera:
            - M - partner to aktualny proces
            - C - połączono
            - _ - nie połączono

        """
        if self.connected():
            return colored("C", "green", None, ["bold"])
        if self.me:
            return colored("M", "grey", None, ["bold"])
        return "_"

    def connected(self):
        """ Zwraca True, jeśli partner jest połączony. """
        return self.socket is not None

    def time(self):
        """ Zwraca aktualny czas tego partnera. """
        if self.me:
            return Timer.get_time()
        elif self.time_difference is None:
            return None
        else:
            return Timer.get_time() - self.time_difference

    def sync(self, event, initiator, timestamp):
        """
        Właściwa funkcja synchronizująca czasy. W tym samym momencie mogą
        odbywać się dwa procesy synchronizacji -- jeden zainicjowany
        przez nasz proces, a drugi przez proces partnera.

        event - rodzaj zdarzenia synchronizacji. Może to być:
            - S - żądanie synchronizacji
            - R - odpowiedź na żądanie
            - T - podziękowanie za synchronizację

        initiator - kto inicjował daną synchronizację. Może przyjąć wartości "me" lub "they".
        """
        assert initiator == "me" or initiator == "they"
        assert event in ["S", "R", "T"]

        self._current_sync_data[initiator][event] = timestamp
        if event == "T":
            d = self._current_sync_data[initiator]
            latency = (d["T"] - d["S"]) / 2
            d["result"] = d["R"] - latency

            self._sync_data.append(d)
            self.last_latencies.append(latency)
            self.update_connection_stats()
            self.time_difference = d["S"] - d["result"]

    def sync_request(self):
        """ Wysyła żądanie synchronizacji. """
        logging.debug("Sending sync request")
        time = self.send("S")
        self.sync("S", "me", time)

    def process_recv(self):
        """ Odbiera i przetwarza wiadomość od partnera. """
        msg_type, timestamp, time = self._recv()
        logging.debug("Recieved message: {}".format(msg_type))
        if msg_type == "S":
            self.send("R")
            self.sync("S", "they", timestamp)
            self.sync("R", "they", time)
        elif msg_type == "R":
            self.send("T")
            self.sync("R", "me", timestamp)
            self.sync("T", "me", time)
        elif msg_type == "T":
            self.sync("T", "they", timestamp)

    def _recv(self):
        """
        Odbiera wiadomość od partnera.
        Zwraca krotkę (msg_type, timestamp, local_time).
        """
        logging.debug("Recv!")
        msgtype = self.socket.recv(1)
        msglen = 4

        chunks = b""
        bytes_recd = 0
        while bytes_recd < msglen:
            chunk = self.socket.recv(min(msglen - bytes_recd, msglen))
            if chunk == '':
                self.close_connection()
                return ''
            chunks += chunk
            bytes_recd = bytes_recd + len(chunk)
        result = struct.unpack("f", chunks)
        assert len(result) == 1
        time = Timer.get_time()
        return (msgtype.decode("ascii"),
                result[0],
                time)

    def send(self, msg_type):
        """
        Wysyła wiadomość typu msg_type z doklejonym czasem.
        Zwraca czas wykonania synchronizacji.
        """
        logging.debug("Sending '{}' to {}".format(msg_type, self.host_name))
        assert len(msg_type) == 1
        time = Timer.get_time()
        self._send(msg_type.encode("ascii") + struct.pack("f", time))
        return time

    def _send(self, msg):
        """
        Funkcja pomocnicza. Wysyła podaną wiadomość do partnera.
        """
        totalsent = 0
        try:
            while totalsent < len(msg):
                sent = self.socket.send(msg[totalsent:])
                logging.debug("sent {} bytes".format(sent))
                if sent == 0:
                    self.close_connection()
                totalsent = totalsent + sent
        except socket.error:
            logging.warning("Socket died")
            self.close_connection()

    def __str__(self):

        time_color = "red" if self.me else "green"

        return (self.get_status() +
                " {} {} ({}...) {} ({}), Avg Time = {}, CQ = {}".format(
                    self.ip,
                    self.host_name,
                    self.uuid[:6],
                    colored(self.time(), time_color, None, ["bold"]),
                    self.time_difference,
                    round(self.connection_average_time, 3),
                    round(self.connection_quality, 3)
            ))

    def __del__(self):
        self.close_connection()
