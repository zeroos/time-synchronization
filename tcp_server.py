import socket
import logging
from threading import Thread


class TCPServer(Thread):
    """
    Klasa służy do obsługi przyjmowanych połączeń od klientów przez TCP/IP.
    """
    def __init__(self, port, accept_callback):
        Thread.__init__(self)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind(('', port))  # listen on all interfaces
        self.socket.listen(5)
        self.start()
        self.accept_callback = accept_callback

    def run(self):
        while True:
            self.recv()

    def recv(self):
        # ready = select.select([self.socket], [], [], 1)
        # if self.socket in ready[0]:
        conn, addr = self.socket.accept()
        self.accept_callback(conn, addr)
        logging.debug("{} connected".format(addr))

    def __del__(self):
        logging.debug("closing server socket")
        self.socket.close()
