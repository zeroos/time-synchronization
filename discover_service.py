import select
import pybonjour


# Copied from django.utils.functional
def curry(_curried_func, *args, **kwargs):
    def _curried(*moreargs, **morekwargs):
        return _curried_func(*(args+moreargs), **dict(kwargs, **morekwargs))
    return _curried


def discover_services(add_callback, remove_callback):  # NOQA
    """
    Funkcja służy do wykrycia innych partnerów przy pomocy Bonjour.
    """
    regtype = "_timesync._tcp"
    timeout = 5
    resolved = []

    def resolve_callback(sdRef, flags, interfaceIndex, errorCode, fullname,
                         hosttarget, port, txtRecord):
        if errorCode == pybonjour.kDNSServiceErr_NoError:
            resolved.append(True)
            name = fullname[:fullname.index('.')]
            ip = name.split("--")[-1].replace('-', '.')
            add_callback(fullname, hosttarget, ip, port)

    def browse_callback(sdRef, flags, interfaceIndex, errorCode, serviceName,
                        regtype, replyDomain):
        if errorCode != pybonjour.kDNSServiceErr_NoError:
            return

        if not (flags & pybonjour.kDNSServiceFlagsAdd):
            remove_callback(serviceName)
            return

        # Service added; resolving

        resolve_sdRef = pybonjour.DNSServiceResolve(0,
                                                    interfaceIndex,
                                                    serviceName,
                                                    regtype,
                                                    replyDomain,
                                                    resolve_callback)

        try:
            while not resolved:
                ready = select.select([resolve_sdRef], [], [], timeout)
                if resolve_sdRef not in ready[0]:
                    raise Exception('Resolve timed out')
                    break
                pybonjour.DNSServiceProcessResult(resolve_sdRef)
            else:
                resolved.pop()
        finally:
            resolve_sdRef.close()

    browse_sdRef = pybonjour.DNSServiceBrowse(regtype=regtype,
                                              callBack=browse_callback)

    def refresh():
        ready = select.select([browse_sdRef], [], [], 1)
        if browse_sdRef in ready[0]:
            pybonjour.DNSServiceProcessResult(browse_sdRef)

    def close():
        browse_sdRef.close()

    refresh()
    return refresh, close

if __name__ == "__main__":
    """
    Serves as an example, prints a message when a new service becomes
    available or disappears.
    """
    def add_callback(service_name, host_name, port):
        print("Service added:\n" +
              "    service name: {}\n".format(service_name) +
              "    host name:    {}\n".format(host_name) +
              "    port:         {}".format(port))

    def remove_callback(service_name):
        print("Service deleted: {}".format(service_name))

    refresh, close = discover_services(add_callback, remove_callback)

    try:
        while True:
            refresh()
    finally:
        close()
