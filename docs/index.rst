Welcome to Time Synchronization's documentation!
================================================

.. toctree::
   :maxdepth: 2

   design/intro
   design/overview
   design/code
