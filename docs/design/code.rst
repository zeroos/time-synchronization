Dokumentacja kodu
=================


Moduł partnera (peer.py)
------------------------

.. automodule:: peer
   :members:
   :special-members:
   :noindex:

Moduł zegara (timer.py)
-----------------------

.. automodule:: timer
   :members:
   :special-members:
   :noindex:

Moduł zarządcy partnerów (peer_manager.py)
------------------------------------------

.. automodule:: peer_manager
   :members:
   :special-members:
   :noindex:


Moduł serwera TCP (tcp_server.py)
---------------------------------

.. automodule:: tcp_server
   :members:
   :special-members:
   :noindex:


Moduły zarządzania usługami 
---------------------------

Publikacja usługi (publish_service.py)
**************************************

.. automodule:: publish_service
   :members:
   :special-members:
   :noindex:


Wykrywanie usługi (discover_service.py)
***************************************

.. automodule:: discover_service
   :members:
   :special-members:
   :noindex:

