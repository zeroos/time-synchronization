Przegląd systemu
================


Definicje
---------

Partner
*******

Pojedynczy proces biorący udział w synchronizacji zegarów. Udostępnia usługę sieciową umożliwiającą innym partnerom wykrycie go i nawiązanie z nim połączenia.

W dalszej części dokumentu pojęcia *proces* oraz *partner* będą używane zamiennie.

Zegar
*****

W kontekście tego studium pojęcie *zegar* określa zegar logiczny posiadany przez każdego partnera.


Cel synchronizacji
------------------

Podstawowym pytaniem, na które należy odpowiedzieć, jest co tak naprawdę chcemy osiągnąć poprzez synchronizację.
Skoro żaden z procesów nie ma dostępu do czasu wzorcowego, co powinno być docelową wartością zegara poszczególnych partnerów?

W ramach tego studium przyjęliśmy, że konkretna wartość nie jest istotna, ale chcielibyśmy, żeby każdy z partnerów miał ten sam czas.
Proces przebiega mniej więcej w następujący sposób:

* Nowo utworzony proces (partner) rejestruje swoją usługę i ustawia czas swojego
  zegara na 0.
* Co pewien czas sprawdza, czy w sieci nie pojawił się jakiś nowy partner.
* Jeżeli istnieją już jacyś partnerzy, proces nawiązuje z nimi połączenie tak, aby zawsze być połączonym z 2 partnerami.
* Co pewien czas partner wylicza średni czas swoich partnerów i ustawia go jako
  swój czas. Do obliczeń nie są brane bardzo małe wartości (mniejsze niż 15
  sekund).

Oprócz tego każdy partner testuje jakość swojego połączenia z poszczególnymi partnerami. Opóźnienia w komunikacji są brane pod uwagę podczas
obliczania czasu partnera.


Statusy połączenia
------------------


**Niepołączony**

    Partner jest widoczny, lecz nie nawiązano z nim aktywnego połączenia.

**Połączony**

    Partnerzy są połączeni ze sobą i aktywnie wysyłają do siebie komunikaty synchronizujące.

**Ignorowany**

    Partner z jakiegoś powodu został uznany za niezaufanego i dlatego nie będą z nim nawiązywane połączenia.


Komunikacja między partnerami
-----------------------------

.. figure:: img/communication.png
   :scale: 40 %
   :alt: Single exchange
   :align: center

   Pojedyncza wymiana komunikatów między partnerami A i B

Po nawiązaniu ze sobą połączenia partnerzy wysyłają do siebie co jakiś czas komunikaty synchronizujące, z których każdy zawiera aktualną wartość zegara
nadawcy. W ramach tej wymiany dokonywane jest również szacowanie czasu, jaki jest potrzebny na dotarcie komunikatu od jednego partnera do drugiego.
Otrzymane wartości są następnie wykorzystywane do uaktualniania własnego zegara, jak również do obliczania współczynników danego połączenia.


Współczynniki połączenia
------------------------

Każdy partner pamięta zestaw współczynników odnoszący się do poszczególnych partnerów, z którymi jest/był połączony. Współczynniki te mają charakter
informacyjny, ale można je też wykorzystać przy uaktualnianiu wartości zegara, lub podejmowaniu decyzji związanych z nawiązywaniem lub
zrywaniem połączenia z danym partnerem.

Średni czas wymiany
*******************

Współczynnik informujący o średnim czasie potrzebnym na wymianę komunikatów synchronizujących z danym partnerem.

Jakość połączenia
*****************

Współczynnik obliczany na podstawie opóźnień występujących podczas wymiany komunikatów synchronizujących z określonym partnerem. Jest to odchylenie
standardowe wartości czasów poszczególnych wymian (dokładniej to połówek tych czasów - staramy się ustalić czas podróży komunikatu w jedną stronę).
Pod uwagę brana jest ustalona liczba ostatnich czasów (domyślnie 10).

Współczynnik ten informuje nas o tym, jak bardzo poszczególne czasy różnią się między sobą. Mniejsze wartości współczynnika sugerują, że połączenie
z partnerem jest stabilne, a więc na jego wskazaniach zegara można bardziej polegać.


Parametry partnera
------------------

**Minimum połączeń**

    Minimalna liczba partnerów, z którymi proces ma spróbować się połączyć i synchronizować.

**Minimalny czas połączenia**

    Liczba sekund, przez które partner musi utrzymywać połączenie z innymi partnerami.

**Maksymalny czas ciszy**

    Wartość określająca, po ilu sekundach partner decyduje się na zerwanie połączenia z innym partnerem, który z jakiegoś powodu przestał wysyłać
    komunikaty synchronizujące.

