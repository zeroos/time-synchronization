Wstęp
=====

Cel studium
-----------

Doświadczenie zostało przeprowadzone w ramach przedmiotu "Systemy rozproszone"
prowadzonego przez Zdzisława Płoskiego na Uniwersytecie Wrocławskim w roku akademickim 2013/2014. 

Celem studium jest zbadanie, w jaki sposób i z jakim rezultatem można zsynchronizować
zegary logiczne procesów znajdujących się w sieci lokalnej. Poszczególne procesy
są traktowane jako partnerzy (ang. *peers*), tzn. nie istnieją wyszczególnione procesy,
które pełniłyby rolę serwera czasu. Komunikacja między partnerami odbywa się za pomocą
protokołu TCP/IP.


Wykorzystane narzędzia
----------------------

Program został napisany w języku Python 3. Dodatkowo wykorzystana została biblioteka
`pybonjour <http://code.google.com/p/pybonjour/>`_ ułatwiająca rejestrowanie i wykrywanie usług w sieci.


Wymagania i sposób uruchomienia programu
----------------------------------------

Aktualną wersję programu można znaleźć na stronie
`http://bitbucket.org/zeroos/time-synchronization <http://bitbucket.org/zeroos/time-synchronization/>`_.

Program do poprawnego działania wymaga działającego serwera Bonjour/Avahi oraz
interpretera języka Python w wersji co najmniej 3.4. Wszystkie dodatkowe moduły
są razem z nim dostarczone.

Aby uruchomić program, wystarczy użyć następującej komendy:

.. code-block:: bash

    $ python3 main.py

Dodatkowo podczas uruchamiania programu możemy jako pierwszy argument podać port,
na którym ma on oczekiwać połączeń. Dzięki temu na jednym komputerze można
uruchomić wiele instancji programu.

Np. aby program nasłuchiwał na porcie 12331 wywołujemy:

.. code-block:: bash

    $ python3 main.py 12331

