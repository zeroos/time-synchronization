
# Ten skrypt sluży do automatycznego pobrania projektu i uruchomienia go

set -x

cd /tmp
git clone https://zeroos@bitbucket.org/zeroos/time-synchronization.git
# python -m venv time-synchronization-venv
# source ./time-synchronization-venv/bin/activate
cd time-synchronization
# pip install -r requirements.txt
python3 main.py

set +x
