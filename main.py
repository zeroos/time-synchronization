import logging
import sys

from discover_service import discover_services
from peer_manager import PeerManager
from publish_service import publish_service
from tcp_server import TCPServer
from timer import Timer


logging.basicConfig(filename='/tmp/time_synchronization.log',
                    level=logging.DEBUG)


def main(listen_port):
    name, sdref = publish_service(listen_port)
    peers = PeerManager(name)
    TCPServer(listen_port, peers.get_accept_callback())
    refresh, close = discover_services(peers.get_add_callback(),
                                       peers.get_remove_callback())
    t = Timer()
    t.start()
    try:
        while True:
            refresh()
            peers.refresh()
            print(chr(27) + "[2J")
            print(peers)
    finally:
        close()


if __name__ == "__main__":
    if(len(sys.argv) > 1):
        main(int(sys.argv[1]))
    else:
        main(12345)
